import pymysql
import pandas as pd
from decimal import *
import xlwings as xw
import datetime

class MysqlWriteExcel():
    def getDBData(self,connect_id,sql):
        connect = pymysql.connect(host='IP', user='用户名', passwd='密码', db='数据库名', port=端口,charset='utf8')
        cursor = connect.cursor()
        cursor.execute('select * from t_reportETL_db_info where id=%s',connect_id)
        dbconfig = cursor.fetchall()
        # 获取对应数据库信息
        data_connect = pymysql.connect(host=str(dbconfig[0][1]), user=str(dbconfig[0][3]), passwd=str(dbconfig[0][4]),db=str(dbconfig[0][5]), port=int(dbconfig[0][2]), charset='GBK')
        data_cursor = data_connect.cursor()
        # 执行查询
        # sql是bytes类型，原来采用的是GBK编码，要用GBK解码才能正常显示中文
        sql= sql.decode('gbk')
        data_cursor.execute(sql)
        data = list(data_cursor.fetchall())
        final_data = []
        # 把数据里面的decimal类型改为float,结果保留列表格式，便于后面利用xlwing插入excel
        for row in data:
            new_row = []
            for value in row:
                if isinstance(value, Decimal):
                    new_row.append(Decimal.__float__(value))
                else:
                    new_row.append(value)
            final_data.append(new_row)
        return final_data

    def WriteDataToExcel(self,reportName):
        # 获取报表信息
        connect = pymysql.connect(host='IP', user='用户名', passwd='密码', db='数据库名', port=端口,charset='utf8')
        cursor = connect.cursor()
        cursor.execute('select * from t_report_ETL_info where ReportName=%s',reportName)
        reportInfo = cursor.fetchall()
        # 先遍历查询得到数据
        for i in range(len(reportInfo)):
            reportPath = reportInfo[i][2] + '\\' + reportName
            sheetName = reportInfo[i][3]
            sheetArea = reportInfo[i][4]
            connect_id = reportInfo[i][5]
            sql = reportInfo[i][6]
            final_data=self.getDBData(connect_id,sql)
            #先删除原区域数据，再往excel贴数
            wb = xw.Book(reportPath)
            sht = wb.sheets(sheetName)
            print(reportName,sheetName)
            deleteArea = reportInfo[i][7]
            if not deleteArea is None:
                sht.range(deleteArea).clear_contents()
            sht.range(sheetArea).options(expand="table").value=final_data
            wb.save()
        print('finish one!!!')
        wb.close()

if __name__ == "__main__":
    t = MysqlWriteExcel()
    # 不加条记判断，即每天要更新的报表
    t.WriteDataToExcel("报表1.xlsx")
    t.WriteDataToExcel("报表2.xlsx")
    t.WriteDataToExcel("报表3.xlsx")
    # 周四更新的报表,可根据实际情况设置每周几更新的报表
    if datetime.datetime.now().date().isoweekday() == 4:
        t.WriteDataToExcel("报表4.xlsx")
        t.WriteDataToExcel("报表5.xlsx")

# 后台关闭所有excel进程
import os
cmd = 'taskkill /F /IM excel.exe'
os.system(cmd)