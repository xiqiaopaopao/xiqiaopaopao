from PIL import ImageGrab
import yagmail
import keyring
import pandas as pd
import xlwings as xw

# 将excel截图存为图片
def Excel_To_Pic(file_path,filename,sheet_name,pic_range,pic_rr):
    app = xw.App(visible=False, add_book=False)
    wb = app.books.open(file_path)
    sheet = wb.sheets(sheet_name)
    sheet.range(pic_range).api.CopyPicture()
    sheet.api.Paste()
    pic = sheet.pictures[0]
    pic.api.Copy()
    img = ImageGrab.grabclipboard()
    img.save('D:\python_sth\自动发送邮件\pic\\' + filename + '_' + pic_rr + '.PNG')
    wb.close()
    app.quit()

    img_name = 'D:\python_sth\自动发送邮件\pic\\' + filename + '_' + pic_rr + '.PNG'
    return img_name    # 返回图片路径用于发送邮件

# 登录邮箱,注意授权码不是密码，去邮箱授权申领。最后发送邮件
def Send_Mail(email,filename,file_path,contents):
    yagmail.register('此处邮箱','此处授权码')
    pwd = keyring.get_password('yagmail','此处邮箱')
    yag = yagmail.SMTP(user='此处邮箱',host='smtp.qq.com',password=pwd)
    first_content = ["Dear all","请查收" + filename]
    # contents里面图片格式为：yagmail.inline(图片)
    yag.send(to=email,subject=filename,contents=first_content + list(map(lambda x:yagmail.inline(x) if x[-4:] == '.PNG' else x,contents))
    ,attachments=file_path)

# 读设置文件
file = pd.read_excel('D:\python_sth\自动发送邮件\setup_report.xlsx',sheet_name='Sheet1')
for i in range(file.shape[0]):
    filename = file.loc[i,'rp_name']
    file_path = file.loc[i,'file_path']
    receiver = file.loc[i,'receivers'].split(',')
    pic_info = file.loc[i,'pic_info'].split(']')
    contents = []
    for j in pic_info:
        mail_title = j.split('[')[0]
        pic_r = j.split('[')[1].split(',')
        pic_content = []
        for k in pic_r:
            pic_rr = k.replace(':','')   # 工作表名称+图片区域做图片名称
            sheet_name = k.split('_')[0]
            pic_range = k.split('_')[1]
            print('当前图片区域：',filename,pic_rr)
            img_index = Excel_To_Pic(file_path,filename,sheet_name,pic_range,pic_rr)
            pic_content.append(img_index)
        # 编辑邮件title和图片内容
        m_contents = ['\n',mail_title] + pic_content
        contents = contents + m_contents
    Send_Mail(receiver,filename,file_path,contents)